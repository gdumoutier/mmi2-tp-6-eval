#Evaluation A : Office de Tourisme

- Ajouter un fil d'ariane **Bootstrap 3** aux pages **activities-index.php** et **activities-show.php** et y appliquer les microdatas de type **BreadcrumbList** (https://schema.org/BreadcrumbList). 
- Spécifiez une "title" et une meta description unique pour chaque page. (cf : $title et $description).
- Placez une meta "canonical" sur les pages en ayant besoin.
- Vérifier la sémantique de chaque page !
- Sur la page de contact, utilisez le **HTML5** pour que le formulaire soit dûment rempli avant soumission (attributs : required & pattern). L'adresse mail doit être controlée. Pour l'attribut pattern, trouvez un pattern disponible sur internet.
- Lors du "submit" (voir [submit()](https://api.jquery.com/submit/)) du formulaire de contact, affichez une modale de validation à l'aide de jQuery ou d'un plugins jQuery.
- Appliquez vos connaissances pour rendre les URLs plus "humaines".

----

- BONUS : Ajouter une **favicon** en utilisant [realFaviconGenerator](https://realfavicongenerator.net/).

Bon courage !