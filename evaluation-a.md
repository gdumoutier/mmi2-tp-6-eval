#Evaluation A : Office de Tourisme

- Appliquez des microdatas de type [place](http://schema.org/Place) sur la page **activities-show.php**. Testez ceux-ci avec [l'outil de test des données structurées](https://search.google.com/structured-data/testing-tool) fourni par Google.
	* Les microdatas doivent renseigner le nom du point d'intérêt 
	* Ses coordonnées GPS (GeoCoordinates)
	* Une moyenne d'évaluation (AggregateRating)
	* Un visuel associé (image).
- Spécifiez une "title" et une meta description unique pour chaque page. (cf : $title et $description).
- Placez une meta "canonical" sur les pages en ayant besoin.
- Vérifier la sémantique de chaque page !
- Sur la page de contact, utilisez le **HTML5** pour que le formulaire soit dûment rempli avant soumission (required & pattern). L'adresse mail doit être controlée.
- Lors du "submit" (voir [submit()](https://api.jquery.com/submit/)) du formulaire de contact, affichez une modale de validation à l'aide de jQuery ou d'un plugins jQuery.
- Appliquez vos connaissances pour rendre les URLs plus "humaines".

----

- BONUS : Ajouter une **favicon** en utilisant [realFaviconGenerator](https://realfavicongenerator.net/).

Bon courage !